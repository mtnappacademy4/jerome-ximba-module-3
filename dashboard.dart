import 'package:flutter/material.dart';

import 'package:fuel_smart/fuelConsumption.dart';
import 'package:fuel_smart/logs.dart';
import 'package:fuel_smart/tripEstimate.dart';
import 'package:fuel_smart/Profile.dart';
import 'package:fuel_smart/user.dart';
import 'package:provider/provider.dart';

class Dashboard extends StatelessWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Consumer<User>(builder: (context, user, _) {
      return MaterialApp(
        home: Scaffold(
          appBar: AppBar(
            title: const Text("Dashboard"),
          ),
          body: Center(
              child: Text(
            user.email,
            style: const TextStyle(color: Colors.blueAccent, fontSize: 30.0),
          )),
        ),
      );
    });
  }
}
